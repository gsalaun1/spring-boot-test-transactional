package gsalaun1.springboottesttransactional

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringBootTestTransactionalApplication

fun main(args: Array<String>) {
    runApplication<SpringBootTestTransactionalApplication>(*args)
}
