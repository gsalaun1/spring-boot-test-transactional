package gsalaun1.springboottesttransactional.entity

import javax.persistence.Embeddable
import javax.persistence.OneToMany

@Embeddable
class House(
    val color: String,
    @OneToMany(mappedBy = "player")
    val videogamesystems: List<VideogameSystem> = emptyList()
)
