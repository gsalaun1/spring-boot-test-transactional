package gsalaun1.springboottesttransactional.entity

import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
class VideogameSystem(
    @Id
    val id: UUID = UUID.randomUUID(),
    val name: String,
    @ManyToOne
    @JoinColumn(name = "player_id")
    val player: Player
)
