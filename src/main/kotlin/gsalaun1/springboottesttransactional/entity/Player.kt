package gsalaun1.springboottesttransactional.entity

import java.util.UUID
import javax.persistence.Embedded
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
class Player(
    @Id
    val id: UUID = UUID.randomUUID(),
    @OneToMany(mappedBy = "player")
    val avatars: List<Avatar> = emptyList(),
    @Embedded val house: House
)
