package gsalaun1.springboottesttransactional.entity

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface PlayerRepository : CrudRepository<Player, UUID>
