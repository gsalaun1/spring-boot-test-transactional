package gsalaun1.springboottesttransactional.entity

import java.util.UUID
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
class Avatar(
    @Id
    val id: UUID = UUID.randomUUID(),
    val url: String,
    @ManyToOne
    @JoinColumn(name = "player_id")
    val player: Player
)
