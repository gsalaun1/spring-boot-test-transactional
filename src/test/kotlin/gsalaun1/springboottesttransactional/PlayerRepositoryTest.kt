package gsalaun1.springboottesttransactional

import gsalaun1.springboottesttransactional.entity.House
import gsalaun1.springboottesttransactional.entity.Player
import gsalaun1.springboottesttransactional.entity.PlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class PlayerRepositoryTest {
    @Autowired
    private lateinit var playerRepository: PlayerRepository

    companion object {
        val player = Player(house = House("white"))
    }

    @Test
    fun `should return empty list of video game systems`() {
        val savedPlayer = playerRepository.save(player)

        assertAll(
            Executable { assertThat(savedPlayer.avatars).isEmpty() },
            Executable { assertThat(savedPlayer.house.videogamesystems).isEmpty() }
        )
    }
}
